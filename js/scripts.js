$(document).ready(function(){

    $('a[href^="http"]').attr('target','_blank');
    
    $('.toggle').click(function(){
        $('.overview').toggleClass('open');
    });

    $('.toggle2').click(function(){
        $('.overview').toggleClass('open');
    });
    
    $('.children').click(function(){
        $('.overview').toggleClass('open');
    });
    
});

function hidden_submenu(athis) {
    $(athis).parent().children('ul').slideToggle();
}