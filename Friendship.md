---
layout: page
title: 友情链接
permalink: /friendship.html
---

### 我的

[KindleEr](https://kindleear.jermey.cn/)

[AdGuard Home](https://dns.jermey.cn/)

[AriaNg](http://slides.jermey.cn/AriaNg.html)

### 我的朋友

[一晌贪欢](http://www.mylifej.cn)

### 其他

[博客大全](http://daohang.lusongsong.com)
