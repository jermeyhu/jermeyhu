---
layout: null
---

var CACHE_NAME  = '{{site.time}}'
var urlsToCache =[
  '/',
  '/index.html',
  '/css/main.css',
  '/css/github-gist.css',
  
  '/js/jquery-3.4.1.min.js',
  '/js/scripts.js',
  '/js/highlight.pack.js',
  
  '/img/bg.jpg',
  '/img/icon_xxhdpi.png',
  '/img/icon_xhdpi.png',
  '/img/icon_hdpi.png',
  '/img/icon_mdpi.png',
  '/img/icon_ldpi.png',
  '/img/icon.png',
  '/img/icon_512.png',
  '/img/menu.png',
  '/img/right.png',
  '/img/down.png',
  '/img/close.png',
  
  {% for post in site.posts %}'{{ post.url | prepend: site.baseurl | prepend: site.url }}', 
  {% endfor %}
  
]

self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }

        // IMPORTANT: Clone the request. A request is a stream and
        // can only be consumed once. Since we are consuming this
        // once by cache and once by the browser for fetch, we need
        // to clone the response.
        var fetchRequest = event.request.clone();

        return fetch(fetchRequest).then(
          function(response) {
            // Check if we received a valid response
            if(!response || response.status !== 200 || response.type !== 'basic') {
              return response;
            }

            // IMPORTANT: Clone the response. A response is a stream
            // and because we want the browser to consume the response
            // as well as the cache consuming the response, we need
            // to clone it so we have two streams.
            var responseToCache = response.clone();

            caches.open(CACHE_NAME)
              .then(function(cache) {
                cache.put(event.request, responseToCache);
              });

            return response;
          }
        );
      })
    );
});

self.addEventListener('activate', function(event) {
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.map(function(cacheName) {
          if (CACHE_NAME.indexOf(cacheName) === -1) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});
